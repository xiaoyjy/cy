<?php
/* By: xiaoyjy@gmail.com
 *
 * URL rule: /[module]/[method]/[resouce id].[display]
 * 
 * Default method: get.
 * Most method should be GET/PUT/DELETE/MODIFY
 */

define('CY_HOME'           , dirname(__DIR__));
define('CY_LIB_PATH'       , CY_HOME.'/src');
if(empty($_SERVER['REQUEST_URI']))
{
	exit("cli mode is not supported.");
}

/* parse request uri start
 * -----------------------------------
 */
$request_uri = $_SERVER['REQUEST_URI'];
$p           = strpos($request_uri, '?');
$request_uri = $p !== false ? substr($request_uri, 0, $p) : $request_uri;

$p           = strpos($request_uri, 'index.php/');
if($p !== false)
{
	$_ENV["url_base"] = substr($request_uri, 0, $p-1);
	$request_uri      = substr($request_uri,    $p);
	$_ENV["url_path"] = $_ENV["url_base"].'/index.php';
}
else
{
	$_ENV["url_base"] = $_ENV["url_path"] = substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT']));
}

/* security request uri filter. */
if(preg_match('/(\.\.|\"|\'|<|>)/', $request_uri))
{
	exit("Permission denied."); 
}

/* get display format. */
if(($p = strrpos($request_uri, '.')) !== false)
{
	$tail = substr($request_uri, $p + 1);
	if(preg_match('/^[a-zA-Z0-9]+$/', $tail))
	{
		$_ENV['display']  = $tail; //'html'
		$request_uri = substr($request_uri, 0, $p);
	}
}

empty($_ENV['display']) && $_ENV['display'] = 'html';

include CY_LIB_PATH.'/init.php';

/* custom init. */
if(file_exists(CY_HOME.'/app/init.php'))
{
	include CY_HOME.'/app/init.php';
}


/* get module, id, method. */
$route = new CY_Util_Routes();
$r     = $route->parse($request_uri, $_ENV['display']);
if($r['errno'] === 0)
{
	$entry = new $r['data']['class'];
	$dt    = call_user_func([$entry, $r['data']['method']], $r['data']['id'], $_REQUEST, $_ENV);
}
else
{
	header("HTTP/1.1 404 Not Found");
	$dt = array('errno' => $r['errno'], 'data' => 'Not found.');
}

if(!isset($dt['errno']))
{
	exit("Bad entry return value.");
}

$t = new CY_Util_Output();
$t->assign($dt  );
$t->render(isset($r['data']['view']) ? $r['data']['view'] : NULL);


?>
