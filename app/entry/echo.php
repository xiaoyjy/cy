<?php

class CA_Entry_Echo
{
	function get($entry, $req)
	{
		$entry->session->set_response(200, json_encode($req));
		$entry->session->start_response();
	}

	function async($entry, $req)
	{
		$loop = $entry->session->worker->loop;

		$cli   = stream_socket_client("tcp://www.baidu.com:80");
		$clifd = new cy_fd($cli, $entry->session);

		$clifd->on(CY_ON_CONNECT, function($fd, $r){
			fwrite($fd->client, "GET / HTTP/1.1\r\nHost: www.baidu.com\r\nAccept: */*\r\n\r\n");
		});

		$clifd->on(CY_ON_RESPONSE, function($fd, $r){
			echo fgets($fd->client);
		});

		$clifd->on(CY_ON_ERROR, function($fd, $r){

		var_dump($r);
			$fd->session->start_response();
		});

		$loop->add($clifd);
	}
}

?>
