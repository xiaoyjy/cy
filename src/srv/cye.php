<?php

class CY_Srv_CYE
{
	protected $workers;
	protected $servers;

	protected $srv;
	protected $cmd;

	protected $loop;
	protected $flag;

	protected $status;
	protected $type;
	protected $gid;

	function __construct()
	{
		$this->servers = [];
		$this->workers = [];
		$this->status  = ['pid' => 0, 'time' => time()];
		$this->cmd = implode(" ", $_SERVER['argv']);

		$this->servers = ['stat' => ['number' => 0, 'status' => 0], 'list' => [], 'worker' => []];
	}

	function deamonlize()
	{
		$pid = pcntl_fork();
		if($pid < 0)
		{
			cy_log(CYE_ERROR, "pcntl_fork($key) error.");
			exit(-1);
		}

		if($pid > 0)
		{
			exit(0);
		}

		$d = posix_setsid();
		fclose(STDIN );
		fclose(STDOUT);
		fclose(STDERR);
	}

	function start()
	{
		if(!empty($_ENV['config']['deamonlize']))
		{
			$this->deamonlize();
		}

		cy_i_drop(CY_TYPE_SYS);
		cy_i_init(CY_TYPE_SYS);
		cy_i_set('cye_srv_status', CY_TYPE_SYS, 0);

		$this->loop = new cy_loop(128);
		foreach($_ENV['server'] as $name => $param)
		{ 
			if(empty($param['enable']))
			{
				continue;
			}

			switch($pid = pcntl_fork())
			{
				case -1:
					cy_log(CYE_ERROR, "pcntl_fork($key) error.");
					exit(-1);

				case 0:
					$this->start_worker($name, $param);
					exit(0);

				default:
					break;
			}
		}

		// master process only.
		$this->loop->set_interval(1000, [$this, 'interval_master']);

		// master loop here.
		$this->loop->run();
	}

	function start_worker($name, $param)
	{
		$wkname = isset($param['worker']) ? 'CY_Srv_'.$param['worker'] : "CY_Srv_Http";
		if(!class_exists($wkname))
		{
			exit("unkown server implements ".$wkname."\n");
		}

		$listen = isset($param) ? $param['listen'] : "0.0.0.0:8898";
		cy_i_set('cye_srv_'.$name.'_num'    , CY_TYPE_SYS, 0);
		cy_i_set('cye_srv_'.$name.'_req_num', CY_TYPE_SYS, 0);
 
		$worker = new $wkname($listen, $name, $this->loop, $this);

		$number = isset($param['number']) ? $param['number'] : 4;
		for($i = 0; $i < $number; $i++)
		{
			$pid = $this->fork_worker($name, $worker);
		}

		$this->loop->set_interval(1000, [$this, 'interval_worker']);
		$this->loop->run();
	}

	function interval_worker()
	{
		cy_check_exit();
	}

	function interval_master()
	{
		cy_check_exit();
	}

	function restart()
	{	
		cy_i_set('cye_srv_status', CY_TYPE_SYS, 1);
	}

	function stop()
	{
		cy_i_set('cye_srv_status', CY_TYPE_SYS, 2);
	}

	function fork_worker($name, $worker)
	{
		switch($pid = pcntl_fork())
		{
			case -1:
				cy_log(cye_error, "pcntl_fork($name) error.");
				break;

			case 0:	
				// child process loop here.
				$worker->loop();

				// child process exit normal.
				exit(0);

			default:
				cy_i_inc('cye_srv_'.$name.'_num', CY_TYPE_SYS, 1);
				//$w = [];
				//$w['pw'  ] = $this->loop->child($pid, false, [$this, 'worker_onexit']);
				//$w['key' ] = $key;
				//$w['flag'] = $this->flag;
				//$this->workers[$pid] = $w;
				break;
		}

		return $pid;
	}

	function worker_init()
	{
		/*
		foreach($this->workers as $pid => $value)
		{
			if(empty($value['pw']))
			{
				continue;
			}

			$value['pw']->stop();
		}
		*/

		//if(isset($this->watcher['ta'])) { $this->watcher['ta']->stop();  }
		$this->watcher = [];
		$this->workers = [];

		//$this->loop->stop();

		// change display title.
		//cy_title($this->cmd." [worker $key]");

		register_shutdown_function(array($this->servers['worker'], 'srv_finish'));
	}

	function worker_onexit($pw, $revents)
	{
		cy_log(CYE_DEBUG, "in worker_onexit");
		if(!is_object($pw) && get_class($pw) !== 'EvChild')
		{	
			cy_log(CYE_ERROR, 'error type param, in worker_onexit');
			return;
		}

		$pw->stop();
		$pid = $pw->rpid;
		pcntl_waitpid($pid, $status, WNOHANG);

		$wiexit = pcntl_wifexited($status);
		$status = $pw->rstatus;
		$worker = $this->workers[$pid];
		$key    = $worker['key' ];
		unset($this->workers[$pid]);

		if($this->flag === WKST_RUNNING || $this->flag === WKST_SPAWN)
		{
			$this->worker_fork($key);
		}
		elseif($this->flag === WKST_END)
		{
			$now_num = cy_i_get('cye_srv_'.$key.'_num', CY_TYPE_SYS);
			if($now_num === 0)
			{
				$this->loop->stop();
			}
		}

		cy_i_dec("cye_srv_".$key."_num", CY_TYPE_SYS, 1);
		cy_i_del("cye_srv_lock_".$pid  , CY_TYPE_SYS);
		if($wiexit)
		{
			return;
		}

		/* 子进程没有正常退出, 加保护性代码,防止进程因为被kill而死锁 */
		if($flag === WKST_QUITING)
		{
			cy_log(CYE_TRACE, $pid.' exit, receive master cmd.');
		}
		else
		{
			cy_log(CYE_ERROR, $pid.' is not normal exited.');

		}

		// TCP Server Only
		$stat_lock = cy_i_get($stat_name, CY_TYPE_SYS);
		if($stat_lock)
		{
			cy_unlock('bps_'.$key.'_lock');
		}

		usleep(100000);
	}

	function master_timer($tw)
	{
		if(!is_object($tw) && get_class($tw) !== 'EvTimer')
		{
			cy_log(CYE_ERROR, 'wrong type param called in master_timer');
			return;
		}

		//restart by luohaibin
		if (cy_i_get('cye_srv_status', CY_TYPE_SYS) == 1)
		{
			cy_i_set('cye_srv_status', CY_TYPE_SYS, 0);

			$this->flag = WKST_SPAWN;
			$this->worker_start(); 
			$this->worker_clean();
			$this->flag = WKST_RUNNING; 
		}

		//force stop 
		if (cy_i_get('cye_srv_status', CY_TYPE_SYS) == 2)
		{
			cy_i_set('cye_srv_status', CY_TYPE_SYS, 0);

			//master exit
			$this->flag = WKST_END;

			//worker force terminate
			$this->worker_clean();

			// TODO need wait.
			//$this->loop->stop();
		}

		/* Dead lock detect. */
		/*
		$max = isset($_ENV['config']['max_lock_time']) ? $_ENV['config']['max_lock_time'] : 1;
		$now = time();
		foreach($this->workers as $pid => $worker)
		{
			if(cy_i_get('cye_srv_lock_'.$pid, CY_TYPE_SYS))
			{
				if($this->status['pid'] != $pid)
				{
					$this->status['pid']  = $pid;
					$this->status['time'] = $now;
					break;
				}

				if($now - $this->status['time'] > $max)
				{
					cy_log(CYE_ERROR, 'kill process '.$pid.' who had lock more than '.$max.'s');
					cy_i_set('cye_srv_term_'.$pid, CY_TYPE_SYS, 1);
				}

				break;
			}
		}

		*/
	}

	//only for current sync model of worker
	function worker_clean()
	{
		foreach($this->workers as $pid => $worker)
		{
			if($worker[$flag] == WKST_RUNNING)
			{
				$this->workers[$pid]['flag'] = WKST_QUITING;
				cy_i_set('cye_srv_term_'.$pid, CY_TYPE_SYS, 1);
			}
		}
	}
}

?>
