<?php

class CY_Srv_HTTP
{
	protected $srv;
	protected $srv_fd;

	protected $worker;

	protected $requests = 0;
	protected $max_requests = CY_SRV_MAX_REQUEST; 

	protected $pid = 0; 

	protected $last_request_time = 0;
	protected $task_num          = 0;//client number of dealing with

	static $HTTP_HEADERS = array
		(
		 100 => "100 Continue",
		 200 => "200 OK",
		 201 => "201 Created",
		 204 => "204 No Content",
		 206 => "206 Partial Content",
		 300 => "300 Multiple Choices",
		 301 => "301 Moved Permanently",
		 302 => "302 Found",
		 303 => "303 See Other",
		 304 => "304 Not Modified",
		 307 => "307 Temporary Redirect",
		 400 => "400 Bad Request",
		 401 => "401 Unauthorized",
		 403 => "403 Forbidden",
		 404 => "404 Not Found",
		 405 => "405 Method Not Allowed",
		 406 => "406 Not Acceptable",
		 408 => "408 Request Timeout",
		 410 => "410 Gone",
		 413 => "413 Request Entity Too Large",
		 414 => "414 Request URI Too Long",
		 415 => "415 Unsupported Media Type",
		 416 => "416 Requested Range Not Satisfiable",
		 417 => "417 Expectation Failed",
		 500 => "500 Internal Server Error",
		 501 => "501 Method Not Implemented",
		 503 => "503 Service Unavailable",
		 506 => "506 Variant Also Negotiates"
			 );


	/* do init and accept. */
	function __construct($addr, $name, $loop, $server)
	{
		$ptname = "CY_Srv_Protocol_".$name;
		if(!class_exists($ptname))
		{
			exit("unkown server implements ".$ptname."\n");
		}

		$this->name      = $name  ;
		$this->loop      = $loop  ;
		$this->addr      = $addr  ;
		$this->server    = $server;

		// request counter name
		$this->rnum_name = 'cy_srv_'.$name.'_req_num';

		// new a socket fd and listen
		$this->srv = stream_socket_server($this->addr, $errno, $error, STREAM_SERVER_BIND|STREAM_SERVER_LISTEN);
		if(!$this->srv)
		{
			// TODO notify father to stop process.
			cy_log(CYE_ERROR, $addr.' bind failed, errno '.$errno.' error '.$error);
			exit;
		}
	}

	function __destruct()
	{
		//$this->lp->stop();
	}

	function loop()
	{
		$this->loop->del_interval();

		/* loop init start. */
		$this->srv_fd    = new cy_fd($this->srv, $this, 2 /* type=2 server accept */);
		$this->srv_fd->on(CY_ON_ACCEPT, array($this, 'on_accept'));

		// set block mode
		stream_set_blocking($this->srv, 0);
		stream_set_timeout ($this->srv, 0, 0.8);
		$this->loop->add($this->srv_fd);


		/* 为了防止所有进程在同一时刻全部退出, 增加一个随机量. */
		$this->pid           = posix_getpid();
		mt_srand($this->pid);
		$this->max_requests += mt_rand(0, $this->max_requests/10);
		/* loop init end */

		$this->last_request_time = 0;

		$this->loop->set_interval(100, [$this, 'on_interval']);
		$this->loop->run(); /* start loop here. */
	}

	function on_interval()
	{
		/*
		if($this->requests%10000 == 9999)
		{
			gc_collect_cycles();
		}
		*/
		
		if($this->requests > $this->max_requests)
		{
			// TODO : exit when reach max requests
		}

		cy_check_exit();
		return 0;
	}

	function on_close($fd, $req)
	{
		$this->task_num--;

		$this->loop->del($fd);

		fclose($fd->client);
		return 0;
	}

	function on_timeout($fd, $req)
	{
		// FAST response, system busy.
		$fd->session->on_timeout($fd, $req);
		return 0;
	}

	function on_error($fd, $req)
	{
		//var_dump($fd, $req);
		echo __class__, "\t", __FUNCTION__, "\n";
		$fd->session->on_error($fd, $req);
		return 0;
	}

	function on_accept()
	{
		if($this->task_num > 5 /* todo max active process */)
		{
			/* todo add counter */
			return 0;
		}

		$client = stream_socket_accept($this->srv, 0, $this->client_addr);
		if($client == false)
		{
			cy_log(CYE_WARNING, 'stream_socket_accept '.$this->srv.' errno: '.cy_errno());
			return 0;
		}

		$this->task_num++;
		stream_set_blocking($client, 0);

		$http    = new cy_http_req   ();
		$session = new CY_Srv_Session();
		$fd      = new cy_fd($client, $session, 1 /* server type */, 500000 /* 500ms. */);

		$fd->on          (CY_ON_ERROR     , [$this, 'on_error'   ]);
		$fd->on          (CY_ON_CLOSE     , [$this, 'on_close'   ]);
		$fd->on          (CY_ON_TIMEOUT   , [$this, 'on_timeout' ]);
		$fd->set_handler ($http           , [$this, 'on_request' ],  [$this, 'on_response']);

		$this->loop->add($fd);
		return 0;
	}

	function start_response($fd, $req)
	{
		$this->loop->resume($fd);
	}

	function on_request($fd, $req)
	{
		$this->requests++;

		$fd->session->on_request($fd, $req, $this);
		return 0;
	}

	function on_response($fd, $req)
	{
		$response = $fd->session->get_response();
		$body  = $req->version.' '.self::$HTTP_HEADERS[$response['code']]."\r\n";
		$body .= "Date: ".gmdate("D, d M Y H:i:s T", $req->request_time)."\r\n";
		$body .= "Server: CYD ".CY_CYE_VERSION."\r\n";
		$body .= "Connection: ".($req->keepalive ? 'keep-alive' : 'close')."\r\n";
		$body .= "Content-Type: text/html\r\n";
		if($req->gzip && strlen($response['data']) > 4096)
		{
			$body .= "Content-Encoding: gzip\r\n";
			//$response['data']  = "\x1f\x8b\x08\x00\x00\x00\x00\x00";
			//$response['data'] .= substr(gzcompress($response['data'], 6), 0, -4);
			$response['data'] = gzencode($response['data'], 9);
		}

		$body .= "Content-Length: ".strlen($response['data'])."\r\n";
		$body .= "\r\n";
		$body .= $response['data'];
		$body .= "\r\n";
		if(!fwrite($fd->client, $body))
		{
			return -1;
		}

		return 0;
	}
}

/* vim: set ts=4 sw=4 sts=4 tw=100 noet: */
?>
