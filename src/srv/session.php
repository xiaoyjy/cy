<?php

class CY_Srv_Session
{
	public    $worker ;

	protected $entry  ;
	protected $req    ;
	protected $response;

	function __construct()
	{
		$this->response = ["errno" => 0, "code" => 500, "data" => "unkown error."];
	}

	function start_response()
	{
		$this->worker->start_response($this->entry, $this->req);
	}

	function set_response($code, $data)
	{
		$this->response['code'] = $code;
		$this->response['data'] = $data;
	}

	function get_response()
	{
		return $this->response;
	}

	function on_timeout($fd, $req)
	{
		$this->set_response(500, "System is busy.");
		return 0;
	}

	function on_error($fd, $req)
	{
		echo __class__, "\t", __FUNCTION__, "\n";
		//var_dump($fd, $req);
		return 0;
	}

	function on_request($entry, $req, $worker)
	{
		$this->worker = $worker ;
		$this->entry  = $entry  ;
		$this->req    = $req    ;
		$this->req->env = [];

		/* parse request uri start
		 * -----------------------------------
		 */
		$request_uri = $req->servers['uri'];
		$p           = strpos($request_uri, '?');
		$request_uri = $p !== false ? substr($request_uri, 0, $p) : $request_uri;

		$p           = strpos($request_uri, 'index.php/');
		if($p !== false)
		{
			$this->req->env["url_base"] = substr($request_uri, 0, $p-1);
			$request_uri                = substr($request_uri,    $p);
			$this->req->env["url_path"] = $this->req->env["url_base"].'/index.php';
		}
		else
		{
			$_ENV["url_base"] = $_ENV["url_path"] = $request_uri; //substr(__DIR__, strlen($_SERVER['DOCUMENT_ROOT']));
		}

		/* security request uri filter. */
		if(preg_match('/(\.\.|\"|\'|<|>)/', $request_uri))
		{
			$this->set_response(403, "Permission denied.");
			$this->start_response();
			return 0;
		}

		/* get display format. */
		$this->req->env['display'] = 'html';
		if(($p = strrpos($request_uri, '.')) !== false)
		{
			$tail = substr($request_uri, $p + 1);
			if(preg_match('/^[a-zA-Z0-9]+$/', $tail))
			{
				$this->req->env['display']  = $tail; //'html'
				$request_uri = substr($request_uri, 0, $p);
			}
		}

		/* get module, id, method. */
		$route = new CY_Util_Routes();
		$r     = $route->parse($request_uri, $this->req->env['display']);
		if($r['errno'] !== 0)
		{
			$this->set_response(404, "404 Not Found.");
			$this->start_response();
			return 0;
		}

		/* renew session */
		$this->entry->session = $this;

		call_user_func([new $r['data']['class'], $r['data']['method']], $this->entry, $this->req);
		return 0;
	}

}

?>
