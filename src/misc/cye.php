<?php
if(!extension_loaded("cye")):

trigger_error("cye extension is not extists", E_USER_WARNING);

function cy_i_init(){}
function cy_i_drop(){}
function cy_i_next(){}
function cy_i_info(){}
function cy_i_reset(){}
function cy_i_set(){}

function cy_i_get(){ return 0; }
function cy_i_del(){}
function cy_i_inc(){}
function cy_i_dec(){}

function cy_s_init(){}
function cy_s_drop(){}
function cy_s_next(){}
function cy_s_info(){}
function cy_s_reset(){}

function cy_s_set(){}
function cy_s_get(){ return ""; }
function cy_s_del(){}

function cy_create_lock(){ return NULL }
function cy_lock(){}
function cy_unlock(){}

function cy_errno(){ return 0 }

function cy_title() {  }

function cy_ctl_check(){ return true; }
function cy_ctl_succ (){ }
function cy_ctl_fail (){ }

function cy_split_by_tag(){ exit("cye extension is not load"); }

function cy_unpack() { exit("cye extension is not load"); }
function cy_pack() { exit("cye extension is not load"); }


class CY_HT_Client
{
	public $headers = [];
	public $cookies = [];
	public $get     = [];
	public $post    = [];
	public $request = [];
	public $files   = [];
	public $uri;
	public $path;
	public $query;
	public $version = "HTTP/1.1";
	public $method  = "GET";
	public $body;

	public $expect100 = 0;
}

function cy_http_req_parse($string)
{
	$c = new CY_HT_Client;
	$o = new http\Message($string);
	$c->version = $o->getHttpVersion();
	$c->method  = $o->getRequestMethod();
	$c->headers = $o->getHeaders();
	if(!empty($headers['Content-Length']))
	{
		if(isset($headers['Expect']))
		{
			$c->expect100 = 1;
		}
	}

	$c->keepalive = isset($headers['Connection']     ) && (strcasecmp($headers['Connection'], 'keep-alive') == 0);
	$c->gzip      = isset($headers['Accept-Encoding']) && (strpos    ($headers['Accept-Encoding'], 'gzip') !== false);

	$parts  = parse_url('http://'.$headers['Host'].$o->getRequestUrl());
	$c->uri = $o->getRequestUrl();
	if(!empty($parts['query']))
	{
		$c->query = $query = $parts['query'];
		parse_str($query, $_GET);
	}

	if(!empty($o->getBody()))
	{
		$c->body = $o->getBody();
		if(isset($headers['Content-Type']) && strpos($headers['Content-Type'], 'multipart/form-data') !== false)
		{
			// grab multipart boundary from content type header
			preg_match('/boundary=(.*)$/', $headers['Content-Type'], $matches);

			// content type is probably regular form-encoded
			if(count($matches))
			{
				$boundary = $matches[1];
				$c->post = cy_parse_http_multipart($o->getBody(), $boundary);
			}
		}

		if(!isset($boundary))
		{
			parse_str($o->getBody(), $c->post);
		}
	}

	if(isset($headers['Cookie']))
	{
		$c = new http\Cookie($headers['Cookie']);
		$c->cookies = $c->getCookies(); 
	}

	$c->request = array_merge($c->get, $c->post, $c->cookies);
	return $c;
}

endif;

?>
