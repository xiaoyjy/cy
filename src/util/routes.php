<?php

class CY_Util_Routes
{
	function exists($classname, &$method)
	{
		if(!class_exists($classname))
		{
			$errno = CYE_NOT_FOUND;
		}

		if(!method_exists($classname, $method))
		{
			if(!method_exists($classname, '__call'))
			{
				if(!method_exists($classname, 'get'))
				{
					$errno = CYE_NOT_FOUND;
				}
				else
				{
					$method = 'get';
				}
			}
		}

		return isset($errno) ? $errno : 0;
	}

	function parse($request_uri, $type)
	{
		$request_uri = trim($request_uri, '/');
		$requests    = explode('/', $request_uri);
		$request_num = count($requests);
		$ids         = [];
		$errno       = 0;
		if($request_num > 1)
		{
			$method = $m = array_pop($requests);
			for($i = 0; $i < $request_num; $i++)
			{
				$classname = 'CA_Entry_'.implode('_', $requests);
				if(($errno = $this->exists($classname, $m)) !== 0 && isset($requests[1]))
				{
					array_unshift($ids, $method);
					$method    = $m = array_pop($requests);
					$classname = 'CA_Entry_'.implode('_', $requests);
					continue;
				}

				if($m != $method)
				{
					array_unshift($ids, $method);
					$method = $m;
				}

				break;
			}
		}
		else 
		{
			$classname = !empty($requests[0]) ? 'CA_Entry_'.$requests[0] : 'CA_Entry_Index';
			$method    = 'get';
			$errno     = $this->exists($classname, $method);
		}

		$id   = implode('-', $ids);
		$data = ['class' => $classname, 'method' => $method, 'id' => $id];
		if($type === 'html' || $type === 'php')
		{
			if($errno === 0)
			{
				$path = substr($classname, 9/* sizeof('CA_Entry_') */);
				$path = str_replace('_', '/', strtolower($path));

				$files = array();
				$files[] = CY_HOME.'/app/html/'.$path.'/'.$method.'.php';
				$files[] = CY_HOME.'/app/html/'.$path.'/get.php';
				$files[] = CY_HOME.'/app/html/'.$path.'.php';
				$files[] = CY_HOME.'/app/html/default.php';
				foreach($files as $i => $file)
				{
					if(file_exists($file))
					{
						$data['view'] = $file;
						break;
					}
				}
			}

			if(empty($data['view']))
			{
				$data['view'] = CY_HOME.'/app/html/error.php';
			}
		}

		return ['errno' => $errno, 'data' => $data];
	}


}

?>
